﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BikeRentalCompany.Models;
using System.Diagnostics;
using BikeRentalCompany.Models.ViewModels;

namespace BikeRentalCompany.Controllers
{
    public class OrderController : Controller
    {
        BikeCMSContext db = new BikeCMSContext();

        public ActionResult List()
        {
            //var orders = db.Orders.Include(o => o.Bike).Include(o => o.Client);
            var orders = db.Orders.ToList();
            return View(orders.ToList());
        }

        // GET: Orders
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Orders/Create
        [HttpPost]
        public ActionResult Create(string order_title_new, string order_date_new)
        {
            Debug.WriteLine(order_title_new + order_date_new);

            string query = "insert into orders (ordertitle, orderdate) values (@title, @date)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@title", order_title_new);
            myparams[1] = new SqlParameter("@date", order_date_new);

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if ((id == null) || (db.Orders.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from orders where OrderID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Order myorder = db.Orders.SqlQuery(query, param).FirstOrDefault();
            return View(myorder);

            //OrderEdit orderedit = new OrderEdit();
            //ordereditview.bikes = db.Bikes.ToList();
            //ordereditview.bikes = db.Orders.Find(id);
            //return View(ordereditview);
        }

        // POST: Orders/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, string ordertitle, string orderdate)
        {

            if ((id == null) || (db.Orders.Find(id)) == null)
            {
                return HttpNotFound();
            }

            string query = "update orders set ordertitle=@title, orderdate=@date where OrderID = @id";
            SqlParameter[] myparams = new SqlParameter[3];

            myparams[0] = new SqlParameter("@id", id);
            myparams[1] = new SqlParameter("@title", ordertitle);
            myparams[2] = new SqlParameter("@date", orderdate);

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("Details/" + id);

            //string query = "update orders set ordertitle=@title, orderdate=@date, Bike_bikeId=@bid where OrderID = @id";
            //SqlParameter[] myparams = new SqlParameter[4];
            //myparams[0] = new SqlParameter("@title", ordertitle);
            //myparams[1] = new SqlParameter("@date", orderdate);
            //myparams[2] = new SqlParameter("@bid", Bike_bikeId);
            //myparams[3] = new SqlParameter("@id", id);
            //db.Database.ExecuteSqlCommand(query, myparams);
            //return RedirectToAction("Details/" + id);
        }

        public ActionResult Details(int? id)
        {
            if ((id == null) || (db.Orders.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from orders where OrderID = @id";
            SqlParameter param = new SqlParameter("@id", id);
            Order ordertodetails = db.Orders.Find(id);

            return View(ordertodetails);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Orders.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from orders where OrderID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            return RedirectToAction("List");

        }

    }
}

