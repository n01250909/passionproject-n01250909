﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BikeRentalCompany.Models;
using System.Diagnostics;
using System.IO;
using System.Data.SqlClient;

namespace BikeRentalCompany.Controllers
{
    public class BikeController : Controller
    {
        BikeCMSContext db = new BikeCMSContext();

        public ActionResult List()
        {
            var bikes = db.Bikes.ToList();
            return View(bikes.ToList());
        }

        // GET: Bikes
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        // GET: Bikes/Details/5
        public ActionResult Details(int? id)
        {
            if ((id == null) || (db.Bikes.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from bikes where bikeId = @id";
            SqlParameter param = new SqlParameter("@id", id);
            Bike biketodetails = db.Bikes.Find(id);
            return View(biketodetails);
        }

        // GET: Bikes/Create
        public ActionResult Create()
        {
            return View();
        }
  
        // POST: Bikes/Create
        [HttpPost]
        public ActionResult Create(string bike_title_new, string bike_type_new, string bike_description_new, string bike_price_new, string bike_orders_new)
        {
            Debug.WriteLine(bike_title_new + bike_type_new + bike_description_new + bike_price_new + bike_price_new + bike_orders_new);
            string query = "insert into bikes (title, type, description, price, orders) values (@title, @type, @description, @price, @orders)";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@title", bike_title_new);
            myparams[1] = new SqlParameter("@type", bike_type_new);
            myparams[2] = new SqlParameter("@description", bike_description_new);
            myparams[3] = new SqlParameter("@price", bike_price_new);
            myparams[4] = new SqlParameter("@orders", bike_orders_new);
            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");
        }

        // GET: Bikes/Edit/5
        public ActionResult Edit(int? id)
        {
            if ((id == null) || (db.Bikes.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from bikes where bikeId=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Bike mybike = db.Bikes.SqlQuery(query, param).FirstOrDefault();
            return View(mybike);
        }

        // POST: Bikes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, string title, string type, string description, int price, string orders)
        {
            if ((id == null) || (db.Bikes.Find(id)) == null)
            {
                return HttpNotFound();
            }

            string query = "update bikes set title=@title, type=@type, description = @description, price = @price, orders = @orders where bikeId = @id";
            SqlParameter[] myparams = new SqlParameter[6];
            myparams[0] = new SqlParameter("@id", id);
            myparams[1] = new SqlParameter("@title", title);
            myparams[2] = new SqlParameter("@type", type);
            myparams[3] = new SqlParameter("@description", description);
            myparams[4] = new SqlParameter("@price", price);
            myparams[5] = new SqlParameter("@orders", orders);
            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("Details/" + id);
        }

        // GET: Bikes/Delete/5
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Bikes.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from bikes where bikeId=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");
        }
    }
}
