﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BikeRentalCompany.Models
{
    public class BikeCMSContext : DbContext
    {
        public BikeCMSContext()
        {

        }
        public DbSet<Bike> Bikes { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}