﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BikeRentalCompany.Models
{
    public class Bike
    {
        [Key]
        public int bikeId { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public int price { get; set; }
        public string orders { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

    }
}