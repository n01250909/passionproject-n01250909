﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BikeRentalCompany.Models
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public string ordertitle { get; set; }
        public string orderdate { get; set; }
        public virtual Bike Bike { get; set; }
    }
}